CREATE DATABASE AgendaAngular;
USE AgendaAngular;

CREATE TABLE Usuario (
  idUsuario INT AUTO_INCREMENT NOT NULL,
  nick VARCHAR(20) NOT NULL,
  contrasena VARCHAR(30) NOT NULL,
  PRIMARY KEY(idUsuario)
);

CREATE TABLE Categoria(
  idCategoria INT AUTO_INCREMENT NOT NULL,
  nombreCategoria VARCHAR(30) NOT NULL,
  PRIMARY KEY(idCategoria)
);

CREATE TABLE Contacto(
  idContacto INT AUTO_INCREMENT NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  apellido VARCHAR(30) NOT NULL,
  telefono VARCHAR(20) NOT NULL,
  direccion VARCHAR(30),
  correo VARCHAR(30),
  idCategoria INT NOT NULL,
  PRIMARY KEY(idContacto),
  FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
);

CREATE TABLE DetalleContacto(
  idDetalleContacto INT AUTO_INCREMENT NOT NULL,
  idUsuario INT NOT NULL,
  idContacto INT NOT NULL,
  PRIMARY KEY(idDetalleContacto),
  FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY(idContacto) REFERENCES Contacto(idContacto)
);

CREATE TABLE Tarea (
  idTarea INT AUTO_INCREMENT NOT NULL,
  titulo VARCHAR(40) NOT NULL,
  descripcion TEXT NOT NULL,
  fechaInicio DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  fechaFinal DATETIME NOT NULL,
  estado VARCHAR(20),
  PRIMARY KEY(idTarea)
);

CREATE TABLE Cita(
  idCita INT AUTO_INCREMENT NOT NULL,
  asunto VARCHAR(50) NOT NULL,
  descripcion TEXT,
  fechaCita DATETIME,
  PRIMARY KEY(idCita)
);

CREATE TABLE DetalleCita(
  idDetalleCita INT AUTO_INCREMENT NOT NULL,
  idUsuario INT NOT NULL,
  idContacto INT NOT NULL,
  idCita INT NOT NULL,
  PRIMARY KEY(idDetalleCita),
  FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY(idContacto) REFERENCES Contacto(idContacto),
  FOREIGN KEY(idCita) REFERENCES Cita(idCita)
);

CREATE TABLE DetalleTarea(
  idDetalleTarea INT AUTO_INCREMENT NOT NULL,
  idUsuario INT NOT NULL,
  idTarea INT NOT NULL,
  PRIMARY KEY(idDetalleTarea),
  FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY(idTarea) REFERENCES Tarea(idTarea)
);

CREATE TABLE DetalleCategoria(
  idDetalleCategoria INT AUTO_INCREMENT NOT NULL,
  idUsuario INT NOT NULL,
  idCategoria INT NOT NULL,
  PRIMARY KEY(idDetalleCategoria),
  FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
);


-- PROCEDIMIENTOS ALMACENADOS PARA USUARIO
DELIMITER $$
CREATE PROCEDURE sp_autenticarUsuario
(IN _nick VARCHAR(50), IN _contrasena VARCHAR(50))
BEGIN
	DECLARE _usuarioExistente INT;
	SET _usuarioExistente = (SELECT Count(*) FROM Usuario WHERE nick = _nick AND contrasena = _contrasena);

	IF(_usuarioExistente != 0) THEN
		SELECT * FROM Usuario WHERE nick = _nick AND contrasena = _contrasena;
	END IF;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_selectUsuario
(IN _idUsuario INT)
BEGIN
  DECLARE _usuarioExistente INT;
  SET _usuarioExistente = (SELECT Count(*) FROM Usuario WHERE idUsuario = _idUsuario);

  IF(_usuarioExistente != 0) THEN
    SELECT * FROM Usuario WHERE idUsuario = _idUsuario;
  END IF;
END;
$$

DROP PROCEDURE IF EXIST sp_insertUsuario;
DELIMITER $$
CREATE PROCEDURE sp_insertUsuario
(IN _nick VARCHAR(50),IN  _contrasena VARCHAR(50))
BEGIN
  DECLARE _usuarioExistente INT;
  SET _usuarioExistente = (SELECT Count(*) FROM Usuario WHERE nick = _nick AND contrasena = _contrasena);

  IF(_usuarioExistente = 0) THEN
    INSERT INTO Usuario(nick, contrasena)
    VALUES (_nick, _contrasena);
  END IF;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_updateUsuario
(IN _idUsuario INT, IN _nick VARCHAR(50), IN _contrasena VARCHAR(50))
BEGIN
  UPDATE Usuario SET nick = _nick, contrasena = _contrasena WHERE idUsuario = _idUsuario;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_deleteUsuario
(IN _idUsuario INT)
BEGIN
  DELETE FROM Usuario WHERE idUsuario = _idUsuario;
END;
$$

-- PROCEDIMIENTOS ALMACENADOS PARA CONTACTO

DELIMITER $$
CREATE PROCEDURE sp_selectContactos
(IN _idUsuario INT)
BEGIN
  SELECT d.idContacto, c.nombre, c.apellido, c.telefono, c.direccion, c.correo,
         c.idCategoria, a.nombreCategoria
  FROM DetalleContacto d
    INNER JOIN Contacto c ON d.idContacto = c.idContacto
    INNER JOIN Categoria a ON c.idCategoria = a.idCategoria
  WHERE d.idUsuario = _idUsuario;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_insertContacto
  (IN _idUsuario INT, IN _nombre VARCHAR(40), _apellido VARCHAR(40),
  _telefono VARCHAR(40), _direccion VARCHAR(40), _correo VARCHAR(40),
  _idCategoria INT)
BEGIN
  DECLARE _obtenerId INT;

  INSERT INTO Contacto(nombre, apellido, telefono, direccion, correo, idCategoria)
  VALUES (_nombre, _apellido, _telefono, _direccion, _correo, _idCategoria);

  SET _obtenerId = (SELECT MAX(idContacto) FROM Contacto);
  INSERT INTO DetalleContacto(idUsuario, idContacto)
  VALUES(_idUsuario, _obtenerId);
END;
$$


DELIMITER $$
CREATE PROCEDURE sp_updateContacto
(IN _idContacto INT, IN _nombre VARCHAR(30), IN _apellido VARCHAR(30),IN _telefono VARCHAR(12),
 IN _direccion VARCHAR(30),IN _correo VARCHAR(40), IN _idCategoria INT)
BEGIN
UPDATE Contacto SET nombre = _nombre, apellido = _apellido,
                    telefono= _telefono, direccion = _direccion,
                    correo = _correo, idCategoria=_idCategoria
                    WHERE idContacto = _idContacto;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_deleteContacto
(IN _idContacto INT)
BEGIN
  DELETE FROM DetalleContacto WHERE idContacto = _idContacto;
  DELETE FROM Contacto WHERE idContacto = _idContacto;
END;
$$

--PROCEDIMIENTOS ALMACENADOS PARA Tarea
DROP PROCEDURE IF EXISTS sp_selectTareas;
DELIMITER $$
CREATE PROCEDURE sp_selectTareas
(IN _idUsuario INT)
BEGIN
  SELECT d.idTarea, t.titulo, t.descripcion, t.fechaInicio, t.fechaFinal, t.estado
  FROM DetalleTarea d
    INNER JOIN Tarea t ON d.idTarea = t.idTarea
  WHERE d.idUsuario = _idUsuario;
END;
$$

DROP PROCEDURE IF EXISTS sp_insertTarea;
DELIMITER $$
CREATE PROCEDURE sp_insertTarea
  (IN _idUsuario INT, IN _titulo VARCHAR(40), _descripcion TEXT,
  _fechaInicio DATETIME, _fechaFinal DATETIME, _estado VARCHAR(40))
BEGIN
  DECLARE _obtenerId INT;
  DECLARE _fechaLocal DATETIME DEFAULT CURRENT_TIMESTAMP();

  INSERT INTO Tarea(titulo, descripcion, fechaInicio, fechaFinal, estado)
  VALUES (_titulo, _descripcion, _fechaLocal, _fechaFinal, _estado);

  SET _obtenerId = (SELECT MAX(idTarea) FROM Tarea);
  INSERT INTO DetalleTarea(idUsuario, idTarea)
  VALUES(_idUsuario, _obtenerId);
END;
$$

DROP PROCEDURE IF EXISTS sp_updateTarea;
DELIMITER $$
CREATE PROCEDURE sp_updateTarea
(IN _idTarea INT, IN _titulo VARCHAR(40), IN _descripcion TEXT, IN _fechaInicio DATETIME,
IN _fechaFinal DATETIME,IN _estado VARCHAR(20))
BEGIN
DECLARE _fechaLocal DATETIME DEFAULT CURRENT_TIMESTAMP();
UPDATE Tarea SET titulo = _titulo, descripcion = _descripcion,
                    fechaInicio = _fechaLocal, fechaFinal= _fechaFinal,
                    estado = _estado
                    WHERE idTarea = _idTarea;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_deleteTarea
(IN _idTarea INT)
BEGIN
  DELETE FROM DetalleTarea WHERE idTarea = _idTarea;
  DELETE FROM Tarea WHERE idTarea = _idTarea;
END;
$$

--PROCEDIMIENTOS PRARA CATEGORIAS
DELIMITER $$
CREATE PROCEDURE sp_selectCategorias
(IN _idUsuario INT)
BEGIN
  SELECT d.idCategoria, c.nombreCategoria
  FROM DetalleCategoria d
    INNER JOIN Categoria c ON d.idCategoria = c.idCategoria
  WHERE d.idUsuario = _idUsuario;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_insertCategoria
  (IN _idUsuario INT, IN _nombreCategoria VARCHAR(40))
BEGIN
  DECLARE _obtenerId INT;

  INSERT INTO Categoria(nombreCategoria)
  VALUES (_nombreCategoria);

  SET _obtenerId = (SELECT MAX(idCategoria) FROM Categoria);
  INSERT INTO DetalleCategoria(idUsuario, idCategoria)
  VALUES(_idUsuario, _obtenerId);
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_updateCategoria
(IN _idCategoria INT, IN _nombreCategoria VARCHAR(30))
BEGIN
UPDATE Categoria SET nombreCategoria = _nombreCategoria
                    WHERE idCategoria = _idCategoria;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_deleteCategoria
(IN _idCategoria INT)
BEGIN
  DELETE FROM DetalleCategoria WHERE idCategoria = _idCategoria;
  DELETE FROM Categoria WHERE idCategoria = _idCategoria;
END
$$

--PROCEDIMIENTOS PARA CITAS
DELIMITER $$
CREATE PROCEDURE sp_selectCitas
(IN _idUsuario INT)
BEGIN
  SELECT d.idCita, ct.asunto, ct.descripcion, ct.fechaCita, c.idContacto, c.nombre
  FROM DetalleCita d
    INNER JOIN Contacto c ON d.idContacto = c.idContacto
    INNER JOIN Cita ct ON d.idCita = ct.idCita
  WHERE d.idUsuario = _idUsuario;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_insertCita
  (IN _idUsuario INT, IN _idContacto INT, IN _asunto VARCHAR(50), IN _descripcion TEXT,
   IN _fechaCita DATETIME)
BEGIN
  DECLARE _obtenerId INT;

  INSERT INTO Cita(asunto, descripcion, fechaCita)
  VALUES (_asunto, _descripcion, _fechaCita);

  SET _obtenerId = (SELECT MAX(idCita) FROM Cita);
  INSERT INTO DetalleCita(idUsuario, idContacto, idCita)
  VALUES (_idUsuario, _idContacto, _obtenerId);
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_updateCita
(IN _idCita INT, IN _asunto VARCHAR(50), IN _descripcion TEXT, IN _fechaCita DATETIME)
BEGIN
  UPDATE Cita SET asunto = _asunto, descripcion = _descripcion,
                  fechaCita = _fechaCita
                  WHERE idCita = _idCita;
END;
$$

DELIMITER $$
CREATE PROCEDURE sp_deleteCita
(IN _idCita INT)
BEGIN
  DELETE FROM DetalleCita WHERE idCita = _idCita;
  DELETE FROM Cita WHERE idCita = _idCita;
END;
$$
