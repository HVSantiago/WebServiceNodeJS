var database = require('../config/database.config');
var tarea = {}

tarea.select = function (idUsuario, callback) {
  if (database) {
    database.query('CALL sp_selectTareas(?)', idUsuario,
    function (error, resultados) {
      if (error) {
          throw error;
      } else {
          callback(resultados[0]);
      }
    });
  }
}

tarea.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertTarea(?,?,?,?,?,?)",
    [data.idUsuario, data.titulo, data.descripcion, data.fechaInicio, data.fechaFinal, data.estado],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.update = function(data, callback) {
  if(database) {
    database.query('CALL sp_updateTarea(?,?,?,?,?,?)',
    [data.idTarea, data.titulo, data.descripcion, data.fechaInicio, data.fechaFinal, data.estado],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(resultado[0]);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.delete = function(idTarea, callback) {
  if(database) {
    database.query('CALL sp_deleteTarea(?)', idTarea,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAl

module.exports = tarea;
