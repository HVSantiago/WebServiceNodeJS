var database = require("../config/database.config");
var Categoria = {};

Categoria.select = function(idUsuario, callback) {
  if(database) {
		database.query('CALL sp_selectCategorias(?)', idUsuario,
    function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Categoria.find = function(idCategoria, callback) {
  if(database) {
		database.query('SELECT * FROM Categoria WHERE idCategoria=?', idCategoria, function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Categoria.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_insertCategoria(?,?)',
    [data.idUsuario, data.nombreCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows":resultado.affectedRows});
      }
    });
  }
}

Categoria.update = function(data, callback){
	if(database) {
		database.query('CALL sp_updateCategoria(?,?)',
		[ data.idCategoria, data.nombreCategoria],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Categoria.delete = function(idCategoria, callback) {
	if(database) {
		database.query('CALL sp_deleteCategoria(?)', idCategoria,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Categoria;
