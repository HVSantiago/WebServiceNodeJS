var express = require('express');
var router = express.Router();
var categoria = require('../../model/categoria.model');
var services = require('../../services');

router.get('/categoria/',services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  categoria.select(idUsuario, function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.get('/categoria/:idCategoria', services.verificar, function(req, res, next) {
  var idCategoria = req.params.idCategoria;
  var idUsuario = req.usuario.idUsuario
  categoria.find(idCategoria, function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.post('/categoria', services.verificar, function(req, res, next) {
  var data = {
    idUsuario: req.usuario.idUsuario,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.insert(data, function(resultado){
    if(resultado && resultado.insertId > 0) {
      res.json({"mensaje":"Se ingreso la categoria"});
    } else {
      res.json({"mensaje":"No se ingreso la categoria"});
    }
  });
});


router.put('/categoria/:idCategoria', function(req, res, next){
  var idCategoria = req.params.idCategoria;
  var data = {
    idCategoria : idCategoria,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.update(data, function(resultado){

    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "No se pudo modificar"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "Se modifico correctamente"
      });
    }

  });
});

router.delete('/categoria/:idCategoria', function(req, res, next){
  var idCategoria = req.params.idCategoria;

  categoria.delete(idCategoria, function(resultado){
    if(resultado && resultado.mensaje === "Eliminado") {
      res.json({"mensaje":"Se elimino la categoria correctamente"});
    } else {
      res.json({"mensaje":"Se elimino la categoria"});
    }
  });
});

module.exports = router;
